# Shibboleth

[Shibboleth](https://www.shibboleth.net/) is an identity provider based on SAML.
We recommend you to use our [Shibboleth Docker Image](https://hub.docker.com/r/bbbatscale/shibboleth-sp) since it is preconfigured with the attributes BBB@Scale expects and needs.

## Prerequisites

Shibboleth must be enabled via the environment variable `SHIBBOLETH=enabled`.
The variable `SHIBBOLETH_OVER_HTTP_HEADER` determines weather the shibboleth auth backend should prefix the shibboleth attributes with `HTTP_` to find them in `request.META` or not to prefix them (see the [Django documentation](https://docs.djangoproject.com/en/dev/ref/request-response/#django.http.HttpRequest.META) for more information).
If the web server you use proxies the request to BBB@Scale you most likely have to enable it with `SHIBBOLETH_OVER_HTTP_HEADER=true`.

### Use with Nginx

We recommend you to use our project build [Nginx Docker Image][NginxImage] since it already bundles the [shibboleth integration](https://github.com/nginx-shib/nginx-http-shibboleth).
You have to enable the environment variable `SHIBBOLETH_OVER_HTTP_HEADER=true`, otherwise shibboleth would not work with our [Nginx Docker Image][NginxImage].
Our [Nginx Docker Image][NginxImage] must be started with either the command `shibboleth-http` or `shibboleth-https`, otherwise shibboleth would not work ([see for more information](./nginx.md#nginx_config)).

## Configuration

We recommend using the [dummy configuration](https://gitlab.com/bbbatscale/bbbatscale/-/blob/master/configurations/shibboleth/shibboleth2.xml.dummy) as a base configuration and follow the `TODO` instructions in the file.
For more information on how to configure the shibboleth sp see the [shibboleth wiki](https://wiki.shibboleth.net/confluence/display/SP3/Configuration).

## Troubleshooting

If you set up the project correctly, and you end up in an endless loop in the discovery service (where you select your institution), you're probably behind a load-balancer/proxy and forget to resolve the real client ip via the `X-Forwarded-For` or `X-Real-IP` header.
In case you're using our [Nginx Docker Image][NginxImage], you should check the section [additional server configurations](./nginx.md#additional-server-configurations).

[NginxImage]: https://hub.docker.com/r/bbbatscale/nginx
