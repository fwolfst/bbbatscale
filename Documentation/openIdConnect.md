# Documentation for the BBBatScale OpenID Connect implementation

The OpenID Connect implementation has only been tested with KeyCloak as OpenID Provider.  
While it should generally work with other options, we recommend to use KeyCloak.


### OIDC_ENABLED
Set OIDC_ENABLED to `True` to start using OpenID Connect. 

### OIDC_RP_CLIENT_ID
The OpenID client ID which should be submitted to the OpenID service when necessary. 
This value is typically provided to you by the OpenID service when OpenID credentials are generated for your application.

### OIDC_RP_CLIENT_SECRET
The OpenID client secret which should be provided by your identity provider. 

### OIDC_RP_SCOPES
The space-separated list of OpenID scopes to request.  
OpenID scopes determine the information returned within the OpenID token,  
and thus affect what values can be used as an authenticated user's username.  
To be compliant with OpenID, at least "openid profile" must be requested.  
By default, "openid profile email" is used.  

### OIDC Endpoint Configuration
The [configuration properties](/configurations/env/bbbatscale.env.dummy) for the needed OIDC endpoints (Authorization, Token, userInfo, JWKS end session ) follow the pattern "`OIDC_OP_<Endpoint>_ENDPOINT`".
Those should be provided to you by the identity provider.
For identity providers that implement _OpenID Connect Discovery_, 
this value can be retrieved from the matching property of the JSON file hosted at
https://identity-provider/.well-known/openid-configuration, 
where https://identity-provider is the base URL of the identity provider.
For more details about that Endpoint have a look at [OpenId Provider Metadata](https://openid.net/specs/openid-connect-discovery-1_0.html#ProviderMetadata) or generally at the [spec overview](https://openid.net/developers/specs/)

### OIDC_REFRESH_ENABLED
Users log into BBB@Scale by authenticating with an OIDC provider. While the user is doing things on BBB@Scale,
it’s possible that the account that the user used to authenticate with the OIDC provider was disabled. 
A classic example of this is when a user quits his/her job and their LDAP account is disabled.

However, even if that account was disabled, the user’s account and session on BBB@Scale will continue. 
In this way, a user can lose access to his/her account, but continue to use BBB@Scale.

The SessionRefresh will check to see if the user’s id token has expired and if so, 
redirect to the OIDC provider’s authentication endpoint for a silent re-auth. 
That will redirect back to the page the user was going to.

### OIDC_RENEW_ID_TOKEN_EXPIRY_SECONDS
The length of time it takes for an id token to expire, this settings is only relevant when `OIDC_REFRESH_ENABLED` is set to True

### OIDC_VERIFY_SSL
Controls whether the OpenID Connect client verifies the SSL certificate of the provider response. The default is `True`

# Claims

## Standard Claims

* **given_name**: the first name of the user, defaults to an empty string
* **family_name**: the last name of the user, defaults to an empty string
* **email**: the email of the user, defaults to an emptry string
* **preferred_username**: the username for the user, this is mandatory.

## Custom Claims
Custom claims are used to give more permissions to a user

* **Student/Normal user**: No custom claims are required
* **Teacher/Moderator**: A claim `is_moderator: True` is expected
* **Admin/Superuser**: A claim `is_superuser: True` is expected


## Adjust Claims to your need

If you need to support different claims or don't like our use of the standard claims you can edit
the `create_user` and `update_user` functions in the auth.py.

```python
def create_user(self, claims):
    user = super(openIdBackend, self).create_user(claims)
    moderator_group = Group.objects.get(name='Teacher')
    if claims.get('is_moderator', False):
        moderator_group.user_set.add(user)
    user.is_superuser = claims.get('is_superuser', False)
    user.is_staff = claims.get('is_superuser', False)
    user.first_name = claims.get('given_name', '')
    user.email = claims.get('email', '')
    user.last_name = claims.get('family_name', '')
    user.username = claims.get('preferred_username', '')
    user.save()
    return user

def update_user(self, user, claims):
    moderator_group = Group.objects.get(name='Teacher')
    if claims.get('is_moderator', False):
        moderator_group.user_set.add(user)
    else:
        moderator_group.user_set.remove(user)
    user.is_superuser = claims.get('is_superuser', False)
    user.is_staff = claims.get('is_superuser', False)
    user.first_name = claims.get('given_name', '')
    user.email = claims.get('email', '')
    user.last_name = claims.get('family_name', '')
    user.username = claims.get('preferred_username', '')
    user.save()
    return user
```
