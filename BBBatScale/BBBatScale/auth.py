from django.conf import settings
from django.contrib.auth.models import Group
from mozilla_django_oidc.auth import OIDCAuthenticationBackend


class openIdBackend(OIDCAuthenticationBackend):
    def create_user(self, claims):
        user = super(openIdBackend, self).create_user(claims)
        moderator_group = Group.objects.get_or_create(name=settings.MODERATORS_GROUP)[0]
        if claims.get("is_moderator", False):
            moderator_group.user_set.add(user)
        user.is_superuser = claims.get("is_superuser", False)
        user.is_staff = claims.get("is_superuser", False)
        user.first_name = claims.get("given_name", "")
        user.email = claims.get("email", "")
        user.last_name = claims.get("family_name", "")
        user.username = claims.get("preferred_username", "")
        user.save()
        return user

    def update_user(self, user, claims):
        moderator_group = Group.objects.get_or_create(name=settings.MODERATORS_GROUP)[0]
        if claims.get("is_moderator", False):
            moderator_group.user_set.add(user)
        else:
            moderator_group.user_set.remove(user)
        user.is_superuser = claims.get("is_superuser", False)
        user.is_staff = claims.get("is_superuser", False)
        user.first_name = claims.get("given_name", "")
        user.email = claims.get("email", "")
        user.last_name = claims.get("family_name", "")
        user.username = claims.get("preferred_username", "")
        user.save()
        return user

    def filter_users_by_claims(self, claims):
        """
        Return all users matching the specified username
        If no user is found create_user will be called
        If more than one user (that is two or more user with the
        same username) are found, an error will occur
        If exactly one user is found, update_user will be called
        """
        username = claims.get("preferred_username")
        if not username:
            return self.UserModel.objects.none()
        return self.UserModel.objects.filter(username=username)

    def verify_claims(self, claims):
        """
        if preferred_username is not in claims, the username is missing
        in this case, the user will not be logged into BBB@Scale, even if
        he was able to authenticate to the openid provider
        """
        return "preferred_username" in claims


def provider_logout(request):
    logout_endpoint = settings.OIDC_OP_END_SESSION_ENDPOINT
    return logout_endpoint + "?redirect_uri=" + request.build_absolute_uri("/")
