from core.constants import (
    SCHEDULING_STRATEGY_LEAST_UTILIZATION,
    SCHEDULING_STRATEGY_RANDOM_PICK_FROM_LEAST_UTILIZED,
    SERVER_STATE_UP,
)
from core.models import Room, Server, ServerType, Tenant, get_default_room_config
from django.test import TestCase


class TenantTestCase(TestCase):
    def setUp(self):
        self.fbi = Tenant.objects.create(
            name="Fachbereich Informatik",
            description="Bilden Informatiker aus",
            notifications_emails="FBI Notification E-Mail",
            scheduling_strategy=SCHEDULING_STRATEGY_LEAST_UTILIZATION,
        )
        self.worker = ServerType.objects.get_or_create(name="worker")[0]
        self.bbb_server = Server.objects.create(
            tenant=self.fbi,
            dns="example.org",
            state=SERVER_STATE_UP,
            participant_count_max=10,
            videostream_count_max=2,
        )
        self.bbb_server.server_types.add(self.worker)
        self.room_d19_304 = Room.objects.create(
            tenant=self.fbi,
            server=self.bbb_server,
            name="D19/304",
            participant_count=10,
            videostream_count=5,
            config=get_default_room_config(),
        )

    def test__str__(self):
        c_tenant = Tenant(name="1337")
        self.assertEqual(c_tenant.__str__(), "1337")

    def test_get_current_participant_count(self):
        self.assertEqual(self.fbi.get_current_participant_count(), 10)

    def test_get_utilization(self):
        self.assertEqual(self.fbi.get_utilization(), 7)

    def test_get_utilization_max(self):
        self.assertEqual(self.fbi.get_utilization_max(), 2)

    def test_get_utilization_percent(self):
        self.assertEqual(self.fbi.get_utilization_percent(), 350)

    def test_get_server_for_room(self):
        # alibi
        self.assertEqual(self.fbi.get_server_for_room(), self.bbb_server)

    def test_get_servers_up(self):
        # since there is only server with the state of "server state up"
        # the length of the queryset should be 1
        self.assertEqual(len((self.fbi.get_servers_up())), 1)


class ServerTestCase(TestCase):
    def setUp(self):
        self.fbi = Tenant.objects.create(
            name="Fachbereich Informatik",
        )

        self.bbb_server = Server.objects.create(
            tenant=self.fbi,
            dns="example.org",
            state=SERVER_STATE_UP,
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.room_d19_304 = Room.objects.create(
            tenant=self.fbi,
            server=self.bbb_server,
            name="D19/304",
            participant_count=10,
            videostream_count=5,
            config=get_default_room_config(),
        )

    def test__str__(self):
        self.assertEqual(self.bbb_server.__str__(), "example.org")

    def test_get_utilization(self):
        self.assertEqual(self.bbb_server.get_utilization(), 7.0)

    def test_get_utilization_percent(self):
        self.assertEqual(self.bbb_server.get_utilization_percent(), 350)

    def test_collect_stats(self):
        pass

    def test_get_participant_count(self):
        self.assertEqual(self.bbb_server.get_participant_count(), 10)

    def test_get_videostream_count(self):
        self.assertEqual(self.bbb_server.get_videostream_count(), 5)


class RoomTestCase(TestCase):
    def setUp(self):
        self.fbi = Tenant.objects.create(
            name="Fachbereich Informatik",
        )

        self.bbb_server = Server.objects.create(
            tenant=self.fbi,
            dns="bbb.example.org",
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.room_d19_304 = Room.objects.create(
            tenant=self.fbi,
            server=self.bbb_server,
            name="D19/304",
            participant_count=10,
            videostream_count=5,
            config=get_default_room_config(),
        )

    def test__str__(self):
        self.assertEqual(self.room_d19_304.__str__(), "D19/304")

    def test_get_participants_current(self):
        self.assertEqual(self.room_d19_304.get_participants_current(), 10)

    def test_get_total_instantiation_counter(self):
        pass

    def test_is_meeting_running(self):
        pass

    def test_get_meeting_infos(self):
        pass


class SchedulingLeastUtilizedRandomPickTestCase(TestCase):
    def setUp(self):
        self.fbi = Tenant.objects.create(
            name="Fachbereich Informatik",
            description="Bilden Informatiker aus",
            notifications_emails="FBI Notification E-Mail",
            scheduling_strategy=SCHEDULING_STRATEGY_RANDOM_PICK_FROM_LEAST_UTILIZED,
        )
        self.worker = ServerType.objects.get_or_create(name="worker")[0]

        self.bbb_server_01 = Server.objects.create(
            tenant=self.fbi,
            dns="bbb01.example.org",
            state=SERVER_STATE_UP,
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.room_01_01 = Room.objects.create(
            tenant=self.fbi,
            server=self.bbb_server_01,
            name="room_01_01",
            participant_count=10,
            videostream_count=5,
            config=get_default_room_config(),
        )

        self.room_01_02 = Room.objects.create(
            tenant=self.fbi,
            server=self.bbb_server_01,
            name="room_01_02",
            participant_count=10,
            videostream_count=5,
            config=get_default_room_config(),
        )

        self.room_01_03 = Room.objects.create(
            tenant=self.fbi,
            server=self.bbb_server_01,
            name="room_01_03",
            participant_count=10,
            videostream_count=5,
            config=get_default_room_config(),
        )

        self.bbb_server_02 = Server.objects.create(
            tenant=self.fbi,
            dns="bbb02.example.org",
            state=SERVER_STATE_UP,
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.room_02_01 = Room.objects.create(
            tenant=self.fbi,
            server=self.bbb_server_02,
            name="room_02_01",
            participant_count=10,
            videostream_count=5,
            config=get_default_room_config(),
        )

        self.room_02_02 = Room.objects.create(
            tenant=self.fbi,
            server=self.bbb_server_02,
            name="room_02_02",
            participant_count=10,
            videostream_count=5,
            config=get_default_room_config(),
        )

        self.bbb_server_03 = Server.objects.create(
            tenant=self.fbi,
            dns="bbb03.example.org",
            state=SERVER_STATE_UP,
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.room_03_01 = Room.objects.create(
            tenant=self.fbi,
            server=self.bbb_server_03,
            name="room_03_01",
            participant_count=10,
            videostream_count=5,
            config=get_default_room_config(),
        )

        self.bbb_server_04 = Server.objects.create(
            tenant=self.fbi,
            dns="bbb04.example.org",
            state=SERVER_STATE_UP,
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.room_04_01 = Room.objects.create(
            tenant=self.fbi,
            server=self.bbb_server_04,
            name="room_04_01",
            participant_count=10,
            videostream_count=5,
            config=get_default_room_config(),
        )

        self.bbb_server_05 = Server.objects.create(
            tenant=self.fbi,
            dns="bbb05.example.org",
            state=SERVER_STATE_UP,
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.bbb_server_06 = Server.objects.create(
            tenant=self.fbi,
            dns="bbb06.example.org",
            state=SERVER_STATE_UP,
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.bbb_server_07 = Server.objects.create(
            tenant=self.fbi,
            dns="bbb07.example.org",
            state=SERVER_STATE_UP,
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.bbb_server_08 = Server.objects.create(
            tenant=self.fbi,
            dns="bbb08.example.org",
            state=SERVER_STATE_UP,
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.bbb_server_09 = Server.objects.create(
            tenant=self.fbi,
            dns="bbb09.example.org",
            state=SERVER_STATE_UP,
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.room_09_01 = Room.objects.create(
            tenant=self.fbi,
            server=self.bbb_server_09,
            name="room_09_01",
            participant_count=150,
            videostream_count=5,
            config=get_default_room_config(),
        )

        self.bbb_server_10 = Server.objects.create(
            tenant=self.fbi,
            dns="bbb10.example.org",
            state=SERVER_STATE_UP,
            participant_count_max=10,
            videostream_count_max=2,
        )
        self.bbb_server_01.server_types.add(self.worker)
        self.bbb_server_02.server_types.add(self.worker)
        self.bbb_server_03.server_types.add(self.worker)
        self.bbb_server_04.server_types.add(self.worker)
        self.bbb_server_05.server_types.add(self.worker)
        self.bbb_server_06.server_types.add(self.worker)
        self.bbb_server_07.server_types.add(self.worker)
        self.bbb_server_08.server_types.add(self.worker)
        self.bbb_server_09.server_types.add(self.worker)
        self.bbb_server_10.server_types.add(self.worker)

    def test_get_server_for_room(self):
        selected_server = self.fbi.get_server_for_room()

        zero_utilization_servers = [
            self.bbb_server_10,
            self.bbb_server_08,
            self.bbb_server_07,
            self.bbb_server_06,
            self.bbb_server_05,
        ]
        self.assertTrue(selected_server in zero_utilization_servers)
