import os

import pytest
from core.models import Room, RoomEvent, Server, Tenant, get_default_room_config
from core.services import collect_room_occupancy
from freezegun import freeze_time


@pytest.fixture(scope="function")
def example(db) -> Tenant:
    return Tenant.objects.create(
        name="example",
    )


@pytest.fixture(scope="function")
def example_server(db, example) -> Server:
    return Server.objects.create(
        tenant=example,
        dns="example.org",
        shared_secret="123456789",
    )


file_path = (
    "file://" + os.path.dirname(os.path.realpath(__file__)) + "/.." + "/test_data/SimpleICalCollector_testdata.ics"
)


@pytest.fixture(scope="function")
def room_d14_0204_ical(db, example, example_server) -> Room:
    return Room.objects.create(
        tenant=example,
        server=example_server,
        name="D14/02.04",
        participant_count=10,
        videostream_count=5,
        event_collection_strategy="SimpleICalCollector",
        event_collection_parameters='{"iCal_url": "' + file_path + '", "iCal_encoding": "UTF-8"}',
        config=get_default_room_config(),
    )


@freeze_time("2020-05-25 06:30:00", tz_offset=0)
def test_collect_room_occupancy(room_d14_0204_ical):
    collect_room_occupancy(room_d14_0204_ical.pk)

    room_events = RoomEvent.objects.filter(room=room_d14_0204_ical)
    assert len(room_events) == 4
