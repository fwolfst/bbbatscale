import pytest
from core.models import Server, Tenant
from core.services import create_web_hook_bbb
from core.utils import BigBlueButton


@pytest.fixture(scope="function")
def example(db) -> Tenant:
    return Tenant.objects.create(
        name="example",
    )


@pytest.fixture(scope="function")
def bbb_server(db, example) -> Server:
    return Server.objects.create(
        tenant=example,
        dns="example.org",
        shared_secret="123456789",
        participant_count_max=10,
        videostream_count_max=2,
    )


def test_create_web_hook_bbb(bbb_server, mocker):
    mocker.patch("core.utils.BigBlueButton.create_web_hook", return_value="https://example.org/bigbluebutton/api/")
    bbb = BigBlueButton(bbb_server.dns, bbb_server.shared_secret)
    web_hook_bbb = create_web_hook_bbb(bbb)

    assert web_hook_bbb == "https://example.org/bigbluebutton/api/"
