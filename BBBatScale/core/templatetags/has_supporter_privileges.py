from core.models import User
from django import template
from support_chat.utils import has_supporter_privileges

register = template.Library()


# TODO move to support_chat
#  Problem: base.html won't work if support chat is disabled


@register.filter(name="has_supporter_privileges")
def filter_has_supporter_privileges(user: User) -> bool:
    return has_supporter_privileges(user)


@register.simple_tag(name="has_supporter_privileges")
def do_has_supporter_privileges(user: User) -> bool:
    return has_supporter_privileges(user)
