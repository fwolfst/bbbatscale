from core.views import (
    api,
    create_join,
    groups,
    home_room,
    import_export,
    personal_room,
    recordings,
    room_configurations,
    rooms,
    servers,
    settings,
    statistics,
    tenants,
    users,
)
from django.urls import path

urlpatterns = [
    path("api/callback/bbb/", api.callback_bbb, name="callback_bbb"),
    path("api/recording/postporocessing/done/", api.recording_callback),
    path("api/servers/registration", api.api_server_registration, name="api_server_registration"),
    path("servers/overview", servers.servers_overview, name="servers_overview"),
    path("rooms/overview", rooms.rooms_overview, name="rooms_overview"),
    path("tenants/overview", tenants.tenant_overview, name="tenants_overview"),
    path("rooms/configs/overview", room_configurations.room_config_overview, name="room_configs_overview"),
    path("server/create/", servers.server_create, name="server_create"),
    path("server/delete/<int:server>", servers.server_delete, name="server_delete"),
    path("server/update/<int:server>", servers.server_update, name="server_update"),
    path("room/create/", rooms.room_create, name="room_create"),
    path("room/delete/<int:room>", rooms.room_delete, name="room_delete"),
    path("room/update/<int:room>", rooms.room_update, name="room_update"),
    path("home_room/update<int:home_room>", home_room.home_room_update, name="home_room_update"),
    path("personal_room/create", personal_room.personal_room_create, name="personal_room_create"),
    path("personal_room/update/<int:personal_room>", personal_room.personal_room_update, name="personal_room_update"),
    path("personal_room/delete/<int:personal_room>", personal_room.personal_room_delete, name="personal_room_delete"),
    path("statistics", statistics.statistics, name="statistics"),
    path("importexport", import_export.import_export, name="import_export"),
    path("export/json", import_export.export_download_json, name="export_download_json"),
    path("export/csv", import_export.export_download_csv, name="export_download_csv"),
    path("import/json", import_export.import_upload_json, name="import_upload_json"),
    path("import/csv", import_export.import_upload_csv, name="import_upload_csv"),
    path("room/config/create/", room_configurations.room_config_create, name="room_config_create"),
    path("room/config/delete/<int:room_config>", room_configurations.room_config_delete, name="room_config_delete"),
    path("room/config/update/<int:room_config>", room_configurations.room_config_update, name="room_config_update"),
    path("tenant/create/", tenants.tenant_create, name="tenant_create"),
    path("tenant/delete/<int:tenant>", tenants.tenant_delete, name="tenant_delete"),
    path("tenant/update/<int:tenant>", tenants.tenant_update, name="tenant_update"),
    path("recordings/overview/", recordings.recordings_list, name="recordings_list"),
    path("users/overview/", users.users_overview, name="users_overview"),
    path("user/create/", users.user_create, name="user_create"),
    path("user/change/password/", users.user_change_password, name="change_password"),
    path("user/change/theme/", users.user_change_theme, name="change_theme"),
    path("user/delete/<int:user>", users.user_delete, name="user_delete"),
    path("user/update/<int:user>", users.user_update, name="user_update"),
    path("user/change/password/admin/<int:user>", users.user_change_password_admin, name="user_reset_password_admin"),
    path("groups/overview/", groups.groups_overview, name="groups_overview"),
    path("group/create/", groups.group_create, name="group_create"),
    path("group/delete/<int:group>", groups.group_delete, name="group_delete"),
    path("group/update/<int:group>", groups.group_update, name="group_update"),
    path("join/create/meeting/<str:meeting_id>", create_join.join_or_create_meeting, name="join_or_create_meeting"),
    path("create/meeting/<str:meeting_id>", create_join.create_meeting, name="create_meeting"),
    path("set/username/join/<str:meeting_id>", create_join.session_set_username_pre_join, name="set_username_pre_join"),
    path("join/meeting/<str:meeting_id>", create_join.join_meeting, name="join_meeting"),
    path("join/meeting/anon/", create_join.session_set_username, name="session_set_username"),
    path("meeting/status/<str:meeting_id>", create_join.get_meeting_status, name="get_meeting_status"),
    path("room/meeting/end/<int:room_pk>", rooms.force_end_meeting, name="force_end_meeting"),
    path("<str:tenant_name>/bigbluebutton/api/", api.bbb_initialization_request),
    path("<str:tenant_name>/bigbluebutton/api/join", api.bbb_api_join),
    path("<str:tenant_name>/bigbluebutton/api/create", api.bbb_api_create),
    path("<str:tenant_name>/bigbluebutton/api/getMeetingInfo", api.bbb_api_get_meeting_info),
    path("<str:tenant_name>/bigbluebutton/api/end", api.bbb_api_end),
    path("<str:tenant_name>/bigbluebutton/api/getRecordings", api.bbb_api_get_recordings),
    path("settings", settings.settings_edit, name="settings_edit"),
    path("coowner/user/search", users.user_search_json, name="json_search_user"),
]
