import logging

from core.models import Meeting, Room, Server, Tenant
from core.views import create_view_access_logging_message
from django.shortcuts import render

logger = logging.getLogger(__name__)


def statistics(request):
    logger.info(create_view_access_logging_message(request))

    return render(
        request,
        "statistics.html",
        {
            "servers": Server.objects.all(),
            "rooms": Room.objects.all(),
            "participants_current": Room.get_participants_current(),
            "performed_meetings": Meeting.objects.all().count(),
            "tenants": Tenant.objects.all(),
        },
    )
