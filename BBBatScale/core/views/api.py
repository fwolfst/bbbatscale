import binascii
import hmac
import json
import logging
from datetime import datetime

import xmltodict
from core.constants import ROOM_STATE_ACTIVE
from core.models import ExternalRoom, Meeting, Room, Server, ServerType, Tenant
from core.services import (
    external_get_or_create_room_with_params,
    meeting_create,
    parse_dict_to_xml,
    parse_recordings_from_multiple_sources,
    reset_room,
)
from core.utils import BigBlueButton, validate_bbb_api_call
from core.views import create_view_access_logging_message
from django.conf import settings
from django.db import IntegrityError
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseNotAllowed, JsonResponse
from django.shortcuts import redirect
from django.utils.timezone import now
from django.views.decorators.csrf import csrf_exempt

logger = logging.getLogger(__name__)


@csrf_exempt
def callback_bbb(request):
    logger.info(create_view_access_logging_message(request))

    logger.info("start processing bbb callback request " + request.POST["event"])

    token = request.META["HTTP_AUTHORIZATION"].split(" ")[1]
    event = json.loads(request.POST["event"])[0]["data"]
    domain = request.POST["domain"]
    event_id = event["id"]
    server = Server.objects.get(dns=domain)
    if hmac.compare_digest(token.encode("UTF-8"), server.shared_secret.encode("UTF-8")):
        logger.debug("token validation passed.")

        if event_id in ["meeting-created", "meeting-ended"]:
            meeting_id = event["attributes"]["meeting"]["external-meeting-id"]
            logger.debug("event %s for meeting (id=%s, domain=%s)" % (event_id, meeting_id, domain))
            if event_id == "meeting-created":
                breakout = event["attributes"]["meeting"]["is-breakout"]
                name = event["attributes"]["meeting"]["name"]
                meta = translate_bbb_meta_data(event["attributes"]["meeting"]["metadata"])
                logger.debug("meeting created (domain=%s, id=%s, name=%s)" % (domain, meeting_id, name))
                meta.update(
                    name=name,
                    server=server,
                    tenant=server.tenant,
                    last_running=now(),
                    state=ROOM_STATE_ACTIVE,
                    is_breakout=breakout,
                )
                if event["attributes"]["meeting"]["metadata"].get("bbb-origin", None) == "Moodle":
                    room, created = ExternalRoom.objects.update_or_create(meeting_id=meeting_id, defaults=meta)
                else:
                    room, created = Room.objects.update_or_create(meeting_id=meeting_id, defaults=meta)
                logger.debug(
                    "room %s (name=%s, server=%s, tenant=%s, is_breakout=%s)"
                    % (
                        "created" if created else "updated",
                        room.name,
                        room.server.dns,
                        room.tenant.name,
                        room.is_breakout,
                    )
                )
            if event_id == "meeting-ended":
                room = Room.objects.get(meeting_id=meeting_id)
                logger.debug("meeting ended (domain=%s, id=%s, name=%s)" % (domain, room.meeting_id, room.name))
                if room.is_breakout or room.is_externalroom():
                    logger.info(
                        "room is of type %s and will be deleted (domain=%s, id=%s, name=%s)"
                        % ("breakout" if room.is_breakout else "external", domain, room.meeting_id, room.name)
                    )
                    room.delete()
                else:
                    logger.debug("room will be reset(domain=%s, id=%s, name=%s)" % (domain, room.meeting_id, room.name))
                    reset_room(room.meeting_id, room.name, room.config)

    logger.info("finished processing bbb callback request")
    return JsonResponse({"status": "ok"})


@csrf_exempt
def recording_callback(request):
    logger.info(create_view_access_logging_message(request))

    data = json.loads(request.body)
    if hmac.compare_digest(data["token"].encode("UTF-8"), settings.RECORDINGS_SECRET.encode("UTF-8")):
        if Meeting.objects.filter(pk=data["rooms_meeting_id"]).update(replay_id=data["bbb_meeting_id"]):
            return HttpResponse(status=200)
        else:
            return HttpResponse(status=400)
    else:
        return HttpResponse(status=401)


@csrf_exempt
def api_server_registration(request):  # noqa C901 # McCabe can be a bad proxy for mental context at times
    if request.method != "POST":
        return HttpResponseNotAllowed(["POST"])
    if request.headers.get("Content-Type") != "application/json":
        return HttpResponse(status=415)
    if request.headers.get("X-Bbbatscale-Experimental-Api") != "iacceptmyfate":
        return HttpResponseBadRequest()

    # The request authentication scheme is as for webhooks with the added twist
    # that the MAC key is tenant-specific und thus needs to be looked up first
    # using data from the request.
    # The lookup key is the tenant name transmitted in the X-Bbbatscale-Tenant
    # header.
    sig = request.headers.get("X-Request-Signature")
    if not sig:
        return HttpResponseBadRequest()
    tenant_name = request.headers.get("X-Bbbatscale-Tenant")
    if not tenant_name:
        return HttpResponseBadRequest()
    try:
        timestamp, hex_mac = map(
            lambda x: x[1],
            sorted(filter(lambda x: x[0] in ["t", "v1"], map(lambda x: x.split("=", 1), sig.split(",")))),
        )
        timestamp_int = int(timestamp)
        request_mac = binascii.unhexlify(hex_mac)
        tenant = Tenant.objects.get(name=tenant_name)
    except binascii.Error as e:
        logger.warning("received mis-encoded message authentication code: %s (X-Request-Signature: %s)", e, sig)
        return HttpResponseBadRequest()
    except (ValueError, KeyError) as e:
        logger.info("received bogus X-Request-Signature header: %s (value: %s)", e, sig)
        return HttpResponseBadRequest()
    except Tenant.DoesNotExist:
        logger.warning("received server registration request for non-existent tenant %s", tenant_name)
        return HttpResponseBadRequest()

    mac_key = binascii.unhexlify(tenant.token_registration)
    msg = b"%s.%s" % (timestamp.encode("UTF-8"), request.body)
    expect_mac = hmac.digest(mac_key, msg, "SHA512")
    if not hmac.compare_digest(request_mac, expect_mac):
        return HttpResponse(status=403)

    # Allow for up to an hour of clock drift
    if abs(datetime.now().timestamp() - timestamp_int) > 3600:
        return HttpResponse(status=403)

    # When reaching this point, request.body was successfully authenticated
    # using the shared secret configured for tenant.
    data = json.loads(request.body)
    server, created = Server.objects.update_or_create(
        machine_id=data["machine_id"],
        defaults={
            "dns": data["hostname"],
            "shared_secret": data["shared_secret"],
            "tenant": tenant,
        },
    )
    # For now just add the 'worker' type to all servers enrolled through this
    # endpoint. Going forward we may accept additional types taken from the
    # POSTed request.
    server.server_types.add(ServerType.objects.get_or_create(name="worker")[0])

    if created:
        return HttpResponse(status=201)

    return HttpResponse(status=204)


def bbb_initialization_request(request, tenant_name):
    logger.info(create_view_access_logging_message(request, tenant_name))

    tenant = Tenant.objects.get(name=tenant_name)
    params = request.GET.copy()
    if validate_bbb_api_call("", params, tenant):
        return HttpResponse(
            xmltodict.unparse({"response": {"returncode": "SUCCESS", "version": "0.9"}}), content_type="text/xml"
        )


@csrf_exempt
def bbb_api_create(request, tenant_name):
    logger.info(create_view_access_logging_message(request, tenant_name))

    # Create Meeting and return XML to moodle
    tenant = Tenant.objects.get(name=tenant_name)
    params = request.GET.copy()
    if validate_bbb_api_call("create", params, tenant):
        try:
            room, created = external_get_or_create_room_with_params(params, tenant)
        except IntegrityError:
            room = ExternalRoom.objects.get(meeting_id=params["meetingID"])
        response = meeting_create(params, room).text
        return HttpResponse(response, content_type="text/xml")
    return bbb_checksum_error_xml_response()


def bbb_api_join(request, tenant_name):
    logger.info(create_view_access_logging_message(request, tenant_name))

    # Create Join URL and return to Moodle
    tenant = Tenant.objects.get(name=tenant_name)
    params = request.GET.copy()
    if validate_bbb_api_call("join", params, tenant):
        try:
            room = Room.objects.get(meeting_id=params["meetingID"], tenant=tenant)
            bbb = BigBlueButton(room.server.dns, room.server.shared_secret)
            return redirect(bbb.join(params))
        except Room.DoesNotExist:
            logger.error("Moodle room does not exist in database with ID: {}".format(params["meetingID"]))
            return bbb_meeting_not_found()
    return bbb_checksum_error_xml_response()


def bbb_api_end(request, tenant_name):
    logger.info(create_view_access_logging_message(request, tenant_name))

    # End Meeting
    tenant = Tenant.objects.get(name=tenant_name)
    params = request.GET.copy()
    if validate_bbb_api_call("end", params, tenant):
        try:
            room = Room.objects.get(meeting_id=params["meetingID"], tenant=tenant)
            bbb = BigBlueButton(room.server.dns, room.server.shared_secret)
            response = bbb.end(room.meeting_id, room.moderator_pw)
            return HttpResponse(response.text, content_type="text/xml")
        except Room.DoesNotExist:
            logger.error("Moodle room does not exist in database with ID: {}".format(params["meetingID"]))
            return bbb_meeting_not_found()
    return bbb_checksum_error_xml_response()


def bbb_api_get_meeting_info(request, tenant_name):
    logger.info(create_view_access_logging_message(request, tenant_name))

    # Get Meeting Infos and return XML
    tenant = Tenant.objects.get(name=tenant_name)
    params = request.GET.copy()
    if validate_bbb_api_call("getMeetingInfo", params, tenant):
        try:
            room = Room.objects.get(meeting_id=params["meetingID"], tenant=tenant)
            bbb = BigBlueButton(room.server.dns, room.server.shared_secret)
            response = bbb.get_meeting_infos(room.meeting_id)
            return HttpResponse(response.text, content_type="text/xml")
        except Room.DoesNotExist or AttributeError:
            logger.error("Moodle room does not exist in database with ID: {}".format(params["meetingID"]))
            return bbb_meeting_not_found()
    return bbb_checksum_error_xml_response()


def bbb_api_get_recordings(request, tenant_name):
    logger.info(create_view_access_logging_message(request, tenant_name))

    tenant = Tenant.objects.get(name=tenant_name)
    params = request.GET.copy()
    if validate_bbb_api_call("getRecordings", params, tenant):
        servers = Server.objects.filter(tenant=tenant, server_types__in=[ServerType.objects.get(name="playback").pk])
        recording_response = parse_recordings_from_multiple_sources(servers, params)
        if recording_response:
            return HttpResponse(parse_dict_to_xml(recording_response), content_type="text/xml")
    return HttpResponse(
        xmltodict.unparse(
            {
                "response": {
                    "returncode": "SUCCESS",
                    "recordings": None,
                    "messageKey": "noRecordings",
                    "message": "There are no recordings for the meeting(s).",
                }
            }
        ),
        content_type="text/xml",
    )


def translate_bbb_meta_data(metadata):
    try:
        logger.debug(f"Translation of metadata: {metadata}")
        return {
            "mute_on_start": metadata["muteonstart"] == "True",
            "all_moderator": metadata["allmoderator"] == "True",
            "guest_policy": metadata["guestpolicy"],
            "allow_guest_entry": metadata["allowguestentry"] == "True",
            "access_code": metadata["accesscode"] if metadata["accesscode"] != "None" else None,
            "only_prompt_guests_for_access_code": metadata["onlypromptguestsforaccesscode"] == "True",
            "disable_cam": metadata["disablecam"] == "True",
            "disable_mic": metadata["disablemic"] == "True",
            "disable_note": metadata["disablenote"] == "True",
            "disable_private_chat": metadata["disableprivatechat"] == "True",
            "disable_public_chat": metadata["disablepublicchat"] == "True",
            "allow_recording": metadata["allowrecording"] == "True",
            "url": metadata["url"],
            "maxParticipants": metadata["maxparticipants"] if metadata["maxparticipants"] != "None" else None,
            "streamingUrl": metadata["streamingurl"] if metadata["streamingurl"] != "None" else None,
        }
    except KeyError as e:
        logger.warning(f"Key error while translating metadata: {str(e)}")
        return {}


def bbb_checksum_error_xml_response():
    return HttpResponse(
        xmltodict.unparse(
            {
                "response": {
                    "returncode": "FAILED",
                    "messageKey": "checksumError",
                    "message": "You did not pass the checksum security check",
                }
            }
        ),
        content_type="text/xml",
    )


def bbb_meeting_not_found():
    return HttpResponse(
        xmltodict.unparse(
            {
                "response": {
                    "returncode": "FAILED",
                    "messageKey": "notFound",
                    "message": "We could not find a meeting with that meeting ID",
                }
            }
        ),
        content_type="text/xml",
    )
