import logging

from core.filters import UserFilter
from core.forms import (
    PasswordChangeCrispyForm,
    TenantFilterFormHelper,
    UserForm,
    UserResetPasswordAdminForm,
    UserUpdateForm,
)
from core.models import Theme, User
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required, user_passes_test
from django.db.models import Q
from django.http import HttpRequest, HttpResponse, HttpResponseNotAllowed, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
@staff_member_required
def users_overview(request):
    logger.info(create_view_access_logging_message(request))

    f = UserFilter(request.GET, queryset=User.objects.all())
    f.form.helper = TenantFilterFormHelper
    return render(request, "users_overview.html", {"filter": f})


@login_required
@staff_member_required
def user_create(request):
    logger.info(create_view_access_logging_message(request))

    form = UserForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        user = form.save(commit=False)
        user.set_password(user.password)
        # Custom form validations

        user.save()
        for group in form.cleaned_data.get("groups"):
            user.groups.add(group)
        messages.success(request, _("User {} successfully created.").format(user.username))
        # return to URL
        return redirect("users_overview")
    return render(request, "user_create.html", {"form": form})


@login_required
@staff_member_required
def user_delete(request, user):
    logger.info(create_view_access_logging_message(request, user))

    instance = get_object_or_404(User, pk=user)
    instance.delete()
    messages.success(request, _("User successfully deleted."))
    return HttpResponseRedirect(request.META.get("HTTP_REFERER", "/"))


@login_required
@staff_member_required
def user_update(request, user):
    logger.info(create_view_access_logging_message(request, user))

    instance = get_object_or_404(User, pk=user)

    if request.method == "POST":
        form = UserUpdateForm(request.POST, instance=instance)
        if form.is_valid():
            _user = form.save(commit=False)

            _user.save()
            _user.groups.clear()
            for group in form.cleaned_data.get("groups"):
                _user.groups.add(group)

            messages.success(request, _("User {} successfully updated").format(_user.username))
            return redirect("users_overview")
    form = UserUpdateForm(instance=instance, initial={"groups": instance.groups.all()})
    return render(request, "user_create.html", {"form": form})


@login_required
@user_passes_test(lambda user: user.has_usable_password())
def user_change_password(request):
    logger.info(create_view_access_logging_message(request))

    form = PasswordChangeCrispyForm(request.user, request.POST or None)
    if request.method == "POST" and form.is_valid():
        user = form.save()
        update_session_auth_hash(request, user)
        messages.success(request, _("Password successfully changed."))
        return redirect("home")

    return render(request, "password_change.html", {"form": form})


@login_required
@staff_member_required
@user_passes_test(lambda user: user.has_usable_password())
def user_change_password_admin(request, user):
    logger.info(create_view_access_logging_message(request, user))
    instance = get_object_or_404(User, pk=user)
    form = UserResetPasswordAdminForm(request.POST or None, instance=instance)
    if request.method == "POST":
        if form.is_valid():
            _user = form.save(commit=False)
            if form.cleaned_data["password"] == form.cleaned_data["password_confirm"]:
                _user.set_password(form.cleaned_data["password"])
                _user.save()
                messages.success(request, _("User {} successfully changed password").format(_user.username))
                return redirect("users_overview")
            else:
                messages.error(request, _("Passwords did not match"))
    return render(request, "user_create.html", {"form": form})


@login_required
def user_change_theme(request: HttpRequest) -> HttpResponse:
    logger.info(create_view_access_logging_message(request))

    if request.method == "GET":
        return render(request, "change_theme.html", {"themes": Theme.objects.all().order_by("name")})
    elif request.method == "POST":
        if "theme" in request.POST:
            theme = request.POST["theme"]
            try:
                request.user.theme = Theme.objects.get(pk=theme)
                request.user.save()
            except Theme.DoesNotExist:
                pass
        return redirect("change_theme")
    else:
        return HttpResponseNotAllowed(["GET", "POST"])


@login_required
def user_search_json(request):
    logger.info(create_view_access_logging_message(request))

    q = request.GET.get("q")
    users = []
    if q and len(q) >= 2:
        for user in User.objects.filter(Q(first_name__icontains=q) | Q(last_name__icontains=q) | Q(email__icontains=q)):
            users.append({"id": user.id, "display_name": str(user)})
    return JsonResponse({"users": users})
