import logging

from core.constants import ROOM_STATE_INACTIVE
from core.forms import RoomConfigurationForm
from core.models import RoomConfiguration
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


def room_config_overview(request):
    logger.info(create_view_access_logging_message(request))

    context = {"configs": RoomConfiguration.objects.all()}
    return render(request, "room_configs_overview.html", context)


@login_required
@staff_member_required
def room_config_create(request):
    logger.info(create_view_access_logging_message(request))

    form = RoomConfigurationForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        room_config = form.save(commit=False)
        logger.debug("POST and valid request; room_config=%s", room_config)

        room_config.save()

        messages.success(request, _("Room configuration was created successfully."))
        logger.debug("Room configuration (%S) was created successfully.", room_config)
        # return to URL
        return redirect("room_configs_overview")
    return render(request, "room_configs_create.html", {"form": form})


@login_required
@staff_member_required
def room_config_delete(request, room_config):
    logger.info(create_view_access_logging_message(request, room_config))

    instance = get_object_or_404(RoomConfiguration, pk=room_config)
    logger.debug("Delete room_config=%s", instance)
    instance.delete()

    messages.success(request, _("Room configuration was deleted successfully."))
    logger.debug("Room configuration was deleted successfully.")

    return redirect("room_configs_overview")


@login_required
@staff_member_required
def room_config_update(request, room_config):
    logger.info(create_view_access_logging_message(request, room_config))

    instance = get_object_or_404(RoomConfiguration, pk=room_config)
    form = RoomConfigurationForm(request.POST or None, instance=instance)
    if request.method == "POST" and form.is_valid():
        _room_config = form.save(commit=False)
        _room_config.save()
        _room_config.room_set.filter(state=ROOM_STATE_INACTIVE).update(
            mute_on_start=_room_config.mute_on_start,
            all_moderator=_room_config.all_moderator,
            everyone_can_start=_room_config.everyone_can_start,
            authenticated_user_can_start=_room_config.authenticated_user_can_start,
            guest_policy=_room_config.guest_policy,
            allow_guest_entry=_room_config.allow_guest_entry,
            access_code=_room_config.access_code,
            only_prompt_guests_for_access_code=_room_config.only_prompt_guests_for_access_code,
            disable_cam=_room_config.disable_cam,
            disable_mic=_room_config.disable_mic,
            allow_recording=_room_config.allow_recording,
            disable_private_chat=_room_config.disable_private_chat,
            disable_public_chat=_room_config.disable_public_chat,
            disable_note=_room_config.disable_note,
            url=_room_config.url,
            dialNumber=_room_config.dialNumber,
            logoutUrl=_room_config.logoutUrl,
            welcome_message=_room_config.welcome_message,
            maxParticipants=_room_config.maxParticipants,
            streamingUrl=_room_config.streamingUrl,
        )
        messages.success(request, _("Room configuration was updated successfully."))
        logger.debug("Room configuration was updated successfully for %s", room_config)
        return redirect("room_configs_overview")

    logger.debug("Room configuration was NOT updated successfully for %s", room_config)
    return render(request, "room_configs_create.html", {"form": form})
