import logging

from core.filters import RoomFilter
from core.forms import RoomFilterFormHelper, RoomForm
from core.models import Room
from core.utils import set_room_config
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
@staff_member_required
def rooms_overview(request):
    logger.info(create_view_access_logging_message(request))

    f = RoomFilter(request.GET, queryset=Room.objects.all().prefetch_related("server", "config", "tenant"))
    f.form.helper = RoomFilterFormHelper
    return render(request, "rooms_overview.html", {"filter": f})


@login_required
@staff_member_required
def room_create(request):
    logger.info(create_view_access_logging_message(request))

    form = RoomForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        _room = form.save(commit=False)
        # Custom form validations
        if _room.config:
            set_room_config(_room)
        _room.save()

        messages.success(request, _("Room {} was created successfully.").format(_room.name))
        # return to URL
        return redirect("rooms_overview")
    return render(request, "rooms_create.html", {"form": form})


@login_required
@staff_member_required
def room_delete(request, room):
    logger.info(create_view_access_logging_message(request, room))

    instance = get_object_or_404(Room, pk=room)
    instance.delete()
    messages.success(request, _("Room was deleted successfully."))
    return HttpResponseRedirect(request.META.get("HTTP_REFERER", "/"))


@login_required
@staff_member_required
def room_update(request, room):
    logger.info(create_view_access_logging_message(request, room))

    instance = get_object_or_404(Room, pk=room)
    form = RoomForm(request.POST or None, instance=instance)
    if request.method == "POST" and form.is_valid():
        _room = form.save(commit=False)
        if _room.config:
            set_room_config(_room)
        _room.save()
        messages.success(request, _("Room {} was updated successfully.").format(_room.name))
        return redirect("rooms_overview")
    return render(request, "rooms_create.html", {"form": form})


@login_required
@staff_member_required
def force_end_meeting(request, room_pk):
    logger.info(create_view_access_logging_message(request, room_pk))

    room = get_object_or_404(Room, pk=room_pk)
    if room.end_meeting():
        messages.success(request, _("Meeting in {} successfully ended.").format(room.name))
        logger.debug("Meeting in {} successfully ended.".format(room.name))
    else:
        messages.error(request, _("Error while trying to end meeting in {}.").format(room.name))
        logger.debug("Error while trying to end meeting in {}.".format(room.name))
    return redirect("rooms_overview")
