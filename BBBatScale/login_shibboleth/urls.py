from django.urls import path
from login_shibboleth import views

urlpatterns = [
    path("login/shibboleth", views.login_shibboleth, name="login_shibboleth"),
    path("logout/shibboleth", views.logout_shibboleth, name="logout_shibboleth"),
    path("login/shibboleth/discovery-service", views.discovery_service),
]
