from django.apps import AppConfig


class LoginShibbolethConfig(AppConfig):
    name = "login_shibboleth"
