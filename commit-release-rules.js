"use strict";

const NO_RELEASE = false;
const PATCH = "patch";
const MINOR = "minor";
const MAJOR = "major";

const commonScopes = [
    "api",
    "commands",
    "import_export",
    "lba",
    "ldap",
    "logging",
    "models",
    "nginx",
    "oidc",
    "recordings",
    "shibboleth",
    "support_chat",
    "ui",
    "views",
    "webhooks"
];

const commitReleaseRules = {
    style: { section: null, release: NO_RELEASE, scopes: [null] },
    trans: { section: null, release: PATCH, scopes: ["czech", "galician", "german", "italian", "russian", "spanish"] },
    build: { section: null, release: PATCH, scopes: ["docker", "requirements"] },
    infra: { section: null, release: NO_RELEASE, scopes: ["assets", "ci", "helm"] },
    docs: { section: null, release: NO_RELEASE, scopes: [null, "comparison", ...commonScopes] },
    perf: { section: "Other", release: PATCH, scopes: [null, ...commonScopes] },
    test: { section: null, release: NO_RELEASE, scopes: [null, ...commonScopes] },
    refactor: { section: null, release: PATCH, scopes: [null, ...commonScopes] },
    chore: { section: "Other", release: PATCH, scopes: [null, ...commonScopes] },
    feat: { section: "Features", release: MINOR, scopes: [null, ...commonScopes] },
    fix: { section: "Bug Fixes", release: PATCH, scopes: [null, ...commonScopes] }
};

module.exports = {
    get typeScopeMap() {
        const typeScopeMap = {};
        for (let type in commitReleaseRules) {
            if (Object.prototype.hasOwnProperty.call(commitReleaseRules, type)) {
                typeScopeMap[type] = commitReleaseRules[type].scopes;
            }
        }
        return typeScopeMap;
    },
    get releaseRules() {
        const releaseRules = [];
        for (let type in commitReleaseRules) {
            if (Object.prototype.hasOwnProperty.call(commitReleaseRules, type)) {
                releaseRules.push({
                    type: type,
                    release: commitReleaseRules[type].release
                });
            }
        }
        releaseRules.push({ breaking: true, release: MAJOR }, { revert: true, release: PATCH });
        return releaseRules;
    },
    get sectionMap() {
        const sectionMap = [];
        for (let type in commitReleaseRules) {
            if (Object.prototype.hasOwnProperty.call(commitReleaseRules, type)) {
                const section = {
                    type: type
                };
                if (commitReleaseRules[type].section) {
                    section.section = commitReleaseRules[type].section;
                } else {
                    section.hidden = true;
                }
                sectionMap.push(section);
            }
        }
        sectionMap.push({ breaking: true, release: MAJOR }, { revert: true, release: PATCH });
        return sectionMap;
    }
};
