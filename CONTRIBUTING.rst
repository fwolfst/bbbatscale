Contributing
============

If you have just got time, and you want to contribute: have a look at the tags `Easy <https://gitlab.com/bbbatscale/bbbatscale/-/issues?label_name%5B%5D=Easy>`_, `Cleaning <https://gitlab.com/bbbatscale/bbbatscale/-/issues?label_name%5B%5D=Cleaning>`_, `Documentation <https://gitlab.com/bbbatscale/bbbatscale/-/issues?label_name%5B%5D=Documentation>`_ or `Feature <https://gitlab.com/bbbatscale/bbbatscale/-/issues?label_name%5B%5D=Feature>`_.

Working with issues
-------------------

If you want to contribute something this is great! 
Before starting to write code or do something else: please check, if an issue to that topic already exists please do not create a new one.
If a person is already assigned to an issue you want to do: check up on that person has already started or has some tips for you.

Commit Message (and Merge Request)
----------------------------------

.. image:: https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg
   :alt: Conventional Commits
   :target: https://conventionalcommits.org

| The first line of the commit message will be used to determine the next release version (which follows the `semantic versioning <https://semver.org/>`_) and generating a changelog.
  To support the automatic releasing with `semantic release <https://github.com/semantic-release/semantic-release>`_, the messages must follow the `Conventional Commits <https://www.conventionalcommits.org/>`_ specification.
  Therefore, the commit should be structured as follows:

.. code-block::

  <type>[(<optional scope>)]: <subject>
  <BLANK LINE>
  [optional body]
  <BLANK LINE>
  [optional footer(s)]

Examples
++++++++

.. code-block::

  feat(views): this commit has neither a body nor a footer

.. code-block::

  style: this commit has no scope

  ... but a body

.. code-block::

  chore(models)!: this commit is breaking, thus incrementing the major version

  If no *BREAKING CHANGE* footer has been added (but the exclamation mark in the header before the
  colon), the subject will be used to describe the breaking change.

  To trigger a breaking change, either the exclamation mark or the footer must be provided (both
  together is also possible).

  This body has multiple paragraphs.
  Lines in the commit message (regardless of which) must not exceed 100 characters, such as this line.

  BREAKING CHANGE: the description of the breaking change
  Some-footer: Footer tokens must use `-` instead of whitespaces
  Reviewed-by: Z
  Closes #133

Types and Scopes
++++++++++++++++

+----------+------------------------------------------------------------------------------------+------------------+-------------------+-------------+-----------------------------------+
| Type     | Description                                                                        | Triggers Release | Changelog Section | Needs Scope | Applicable Scopes                 |
+==========+====================================================================================+==================+===================+=============+===================================+
| release  | A release commit only including changes dedicated to a version bump                | No               |                   | |uncheck|   | - *VERSION*                       |
|          | (e.g. update the helm chart app version). *This commit will be created             |                  |                   |             |                                   |
|          | automatically.*                                                                    |                  |                   |             |                                   |
+----------+------------------------------------------------------------------------------------+------------------+-------------------+-------------+-----------------------------------+
| style    | Changes that do not affect the meaning of the code (white-space, formatting, etc). | No               |                   | |uncheck|   |                                   |
+----------+------------------------------------------------------------------------------------+------------------+-------------------+-------------+-----------------------------------+
| trans    | Adding missing or correcting existing translations.                                | Patch            |                   | |check|     | - czech                           |
|          |                                                                                    |                  |                   |             | - galician                        |
|          |                                                                                    |                  |                   |             | - german                          |
|          |                                                                                    |                  |                   |             | - italian                         |
|          |                                                                                    |                  |                   |             | - russian                         |
|          |                                                                                    |                  |                   |             | - spanish                         |
+----------+------------------------------------------------------------------------------------+------------------+-------------------+-------------+-----------------------------------+
| build    | Changes that affect the build system or external dependencies.                     | Patch            |                   | |check|     | - docker                          |
|          |                                                                                    |                  |                   |             | - requirements                    |
+----------+------------------------------------------------------------------------------------+------------------+-------------------+-------------+-----------------------------------+
| infra    | Changes that do not effect the code in any way and should not trigger a release.   | No               |                   | |check|     | - assets                          |
|          |                                                                                    |                  |                   |             | - helm                            |
|          |                                                                                    |                  |                   |             | - ci                              |
+----------+------------------------------------------------------------------------------------+------------------+-------------------+-------------+-----------------------------------+
| docs     | Documentation only changes.                                                        | No               |                   | |uncheck|   | - api                             |
+----------+------------------------------------------------------------------------------------+------------------+-------------------+             | - commands                        |
| perf     | A code change that improves performance.                                           | Patch            | Other             |             | - comparison *(only docs)*        |
+----------+------------------------------------------------------------------------------------+------------------+-------------------+             | - import_export                   |
| test     | Adding missing or correcting existing tests.                                       | No               |                   |             | - lba *(Load Balancer Algorithm)* |
+----------+------------------------------------------------------------------------------------+------------------+-------------------+             | - ldap                            |
| refactor | A code change that neither fixes a bug nor adds a feature.                         | Patch            |                   |             | - logging                         |
+----------+------------------------------------------------------------------------------------+------------------+-------------------+             | - models                          |
| chore    | A code change without adding a new feature, but changing current behaviour.        | Patch            | Other             |             | - nginx                           |
+----------+------------------------------------------------------------------------------------+------------------+-------------------+             | - oidc                            |
| feat     | A new feature.                                                                     | Minor            | Features          |             | - recordings                      |
+----------+------------------------------------------------------------------------------------+------------------+-------------------+             | - shibboleth                      |
| fix      | A bug fix.                                                                         | Patch            | Bug Fixes         |             | - support_chat                    |
|          |                                                                                    |                  |                   |             | - ui                              |
|          |                                                                                    |                  |                   |             | - views                           |
|          |                                                                                    |                  |                   |             | - webhooks                        |
+----------+------------------------------------------------------------------------------------+------------------+-------------------+-------------+-----------------------------------+

.. |uncheck| unicode:: U+2610
.. |check| unicode:: U+2611
